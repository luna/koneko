import logging

import urwid

from ..command.tui import CommandInput
from ..utils import async_

log = logging.getLogger(__name__)


def quick_switcher_widget(parent):
    """Return the main widget for the quick switcher."""
    app = parent.app
    cmd_input = CommandInput()

    def _do_exit():
        cmd_input.set_edit_text("")
        parent.close_pop_up()

    urwid.connect_signal(cmd_input, "command", async_(app, app.on_quick_switcher))
    urwid.connect_signal(cmd_input, "change", async_(app, app.on_quick_switcher_update))
    urwid.connect_signal(cmd_input, "exit", _do_exit)

    div = urwid.Filler(urwid.Divider("─"))
    return urwid.Pile(
        [("pack", cmd_input), urwid.ListBox(urwid.SimpleFocusListWalker([]))]
    )


class QuickSwitcherWrapper(urwid.PopUpLauncher):
    """PopUpLauncher subclass to manage the quick switcher widget
    inside."""

    def __init__(self, app, *args, **kwargs):
        self.app = app
        super().__init__(*args, **kwargs)

    def create_pop_up(self):
        """Create the quick switcher widget. Connects the necessary
        signals back to the main app."""
        log.info("creating pop up")
        qw_widget = quick_switcher_widget(self)

        # wrap it around a linebox!
        return urwid.LineBox(qw_widget, title="quick switcher")

    def get_pop_up_parameters(self):
        return {"left": 10, "top": -20, "overlay_width": 60, "overlay_height": 20}
