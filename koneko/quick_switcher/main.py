import logging
from typing import List, Union, Dict

from fuzzywuzzy import process
from discord import Guild, TextChannel

log = logging.getLogger(__name__)
SearchResult = Union[Guild, TextChannel]


def _insert(res: dict, element: SearchResult, *, key=None):
    key = key or element.name

    counter = sum(1 for _ in filter(lambda key_: key_.startswith(key), res.keys()))

    key = f"{key}-{counter}" if counter > 0 else key
    res[key] = element


class QuickSwitcher:
    """Quick switcher class."""

    def __init__(self, app):
        self.app = app

    @property
    def full_data(self) -> Dict[str, SearchResult]:
        res = {}

        for guild in self.app.client.guilds:
            _insert(res, guild)

        for channel in self.app.client.get_all_channels():
            if not isinstance(channel, TextChannel):
                continue

            if not channel.permissions_for(channel.guild.me).read_messages:
                continue

            _insert(res, channel, key=f"{channel.name}-{channel.guild.name}")

        return res

    async def search(self, search_term: str) -> List[SearchResult]:
        """Search through guilds and channels."""
        if not search_term:
            return []

        # TODO: prepend channels you have pings on
        full_data = self.full_data
        res = process.extract(search_term, full_data.keys(), limit=10)

        # map each key to a SearchResult based on full_data
        return list(map(full_data.get, (key for key, _score in res)))

    @property
    def quick_switcher_pile(self):
        """Return the pile containing the quick switcher
        edit widget and list."""
        return self.app.cmd_input_widget._pop_up_widget.base_widget

    @property
    def qw_components(self):
        pile = self.quick_switcher_pile

        if pile is None:
            return

        # returns tuple containing the edit widget and the listbox
        # with the results
        res = tuple()
        _widget_idxs = [0, 1]

        for idx in _widget_idxs:
            widget, _ = pile.contents[idx]
            res += (widget,)

        return res
