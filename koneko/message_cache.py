import logging
from typing import List
from collections import defaultdict, deque

import discord

log = logging.getLogger(__name__)


class MessageCache:
    """Custom message cache"""

    def __init__(self, app):
        self.app = app
        self._cache = defaultdict(lambda: deque(maxlen=50))

    def push_message(self, message: discord.Message):
        """Insert a message to the cache."""
        if not message.channel:
            return

        self._cache[message.channel.id].append(message)

    def get_messages(self, channel_id: int) -> List[discord.Message]:
        return list(self._cache[channel_id])
