import logging

import discord
from discord import Client as DPYClient

log = logging.getLogger(__name__)


class Client(DPYClient):
    """discord.py client that supports base_url setting, that's all."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # ugly, but works
        base_url = kwargs.get("base_url")
        if base_url:
            discord.http.Route.BASE = base_url

        self.app = kwargs["app"]
