import logging

import discord
import urwid

from .config import Config
from .loop import LoopManager
from .client import Client
from .focus import Focus

from .command.handler import CommandHandler
from .command.errors import CommandError, CommandNotFound
from .quick_switcher.main import QuickSwitcher

from .message_cache import MessageCache
from .data import ClientData

log = logging.getLogger(__name__)


class Koneko:
    """Main application class."""

    def __init__(self):
        self.config = Config()

        self.loop_man = LoopManager()
        self.loop = self.loop_man.loop

        self.client = Client(app=self, base_url=self.config["discord_base_url"])

        # holds focus state
        self.focus = Focus(self)
        self.command_handler = CommandHandler(self)
        self.quick_switcher = QuickSwitcher(self)
        self.client_data = ClientData(self)
        self.cache = MessageCache(self)

        self._tasks = {}

    def start_task(self, name: str, coro):
        """Start a task in the background."""
        log.info("start task %r", name)
        self._tasks[name] = self.loop.create_task(coro)

    def cancel_task(self, name):
        """Cancel a task running in the background."""
        # TODO: ignore if task doesn't exist
        self._tasks[name].cancel()

    def start(self, widget, palette, **kwargs):
        """Start the app."""
        self.loop.create_task(self.client.start(self.config["token"]))

        for (task_name, coro) in kwargs.get("background_tasks", []):
            self.start_task(task_name, coro)

        self.loop_man.urwid_loop(widget, palette).run()

    async def send_msg_cur_focus(self, message: str):
        """Send the current message to the currently focused channel"""
        # TODO: a queue of sorts, same way the discord client does things maybe
        chan = self.client.get_channel(self.focus.channel_id)
        await chan.send(message)

    async def on_command(self, command: str):
        """Handle a full command sent by the client.

        Note that this can be a command or not, depending if the first
        character is a slash or not.
        """
        command = command.strip()

        if command.startswith("/"):
            try:
                await self.command_handler.call(command[1:])
            except CommandNotFound:
                await self.send_msg_cur_focus(command)
            except CommandError as err:
                # TODO: extract and print a traceback to message view
                log.exception("error while running command %r", command)

            return

        await self.send_msg_cur_focus(command)

    @property
    def msg_widget_pile(self):
        """Returns the message-related urwid.Pile widget."""
        # TODO: a method to extract an already started Columns
        # main widget, since the client can in theory still be
        # running another widget as its main one, e.g loading
        # or something else, lol
        columns = self.loop_man.urwid_loop_final.widget
        contents = columns.contents
        pile, _, = contents[0]
        return pile

    @property
    def channel_status_widget(self):
        """Return the widget containing the channel name and topic."""
        widget, _ = self.msg_widget_pile.contents[0]
        return widget

    @property
    def msg_list_widget(self):
        """Return the widget containing the ListBox for the messages."""
        widget, _ = self.msg_widget_pile.contents[2]
        return widget

    @property
    def cmd_input_widget(self):
        """Return the widget that is handling user messages/commands."""
        widget, _ = self.msg_widget_pile.contents[4]
        return widget

    async def on_quick_switcher(self, edit_text):
        """Called when the quick switcher gets an enter key."""
        results = await self.quick_switcher.search(edit_text)
        if not results:
            return

        self.cmd_input_widget.close_pop_up()
        switch_to = results[0]
        log.info("must switch to %r", switch_to)

        switch_id = switch_to.id

        if isinstance(switch_to, discord.Guild):
            self.focus.set_guild_focus(switch_id)
        elif isinstance(switch_to, discord.TextChannel):
            self.focus.set_channel_focus(switch_id)

    async def on_quick_switcher_update(self, _, edit_text):
        """Called when the quick switcher changes text."""
        results = await self.quick_switcher.search(edit_text)
        if not results:
            return

        _qw_edit, qw_listbox = self.quick_switcher.qw_components

        res_widgets = []

        # TODO: make text clickable, e.g button, and link the button
        # click to a focus change on the client.

        # maybe a better idea would decouple this into a
        # widget_from_discord_object function that uses a dict
        # to give better speed guarantees, instead of relying on
        # isinstance() we'd rely on __class__

        div = urwid.Divider()
        for result in results:
            widget = None

            if isinstance(result, discord.Guild):
                widget = urwid.Text(f"g {result.name}")
            elif isinstance(result, discord.TextChannel):
                total = 40
                chanlen = len(result.name)
                guild_text = result.guild.name[:15].rjust(total - chanlen)
                text = f"c {result.name} {guild_text}"
                widget = urwid.Text(text)

            if widget is not None:
                res_widgets.append(widget)
                # res_widgets.append(div)

        # for some reason setting body.contents to a new list doesn't work,
        # at least for a SimpleFocusListWalker, which is sad.
        qw_listbox.body = urwid.SimpleFocusListWalker(res_widgets)
