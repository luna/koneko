from typing import Iterable, Optional, Any


def async_(app, coro_func):
    """Create a wrapper to run coro_func in a separate task

    This exists because urwid signal handlers can't be async."""

    def _inner(*args, **kwargs):
        app.loop.create_task(coro_func(*args, **kwargs))

    return _inner


def try_next(iterable: Iterable[Any]) -> Optional[Any]:
    """Try to get the first element of the iterable via next().

    If it fails, returns None.
    """
    try:
        return next(iterable)
    except StopIteration:
        return None
