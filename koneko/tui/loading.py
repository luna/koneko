import asyncio

import urwid


def loading_widget(_app) -> tuple:
    """Returns a loading widget and a background task to edit the loading
    widget every second"""

    loading_text = urwid.Text("loading", align="center")

    async def task():
        counter = 0

        while True:
            periods = "." * counter
            loading_text.set_text(f"loading{periods}")
            await asyncio.sleep(1)
            counter += 1
            counter %= 4

    return urwid.Filler(loading_text, "middle"), task
