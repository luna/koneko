import urwid

from ..utils import async_
from ..command.tui import CommandInput
from ..quick_switcher.widget import QuickSwitcherWrapper


def guild_list():
    """Return empty guild list."""
    return urwid.ListBox(
        urwid.SimpleFocusListWalker(
            [
                urwid.Text("g 1"),
                urwid.Text("g 2"),
                urwid.Text("g 3"),
                urwid.Text("g 4"),
                urwid.Text("g 5"),
            ]
        )
    )


def channel_list():
    """Return empty channel list."""
    return urwid.ListBox(
        urwid.SimpleFocusListWalker(
            [
                urwid.Text("channel 1"),
                urwid.Text("channel 2"),
                urwid.Text("channel 3"),
                urwid.Text("channel 4"),
                urwid.Text("channel 5"),
            ]
        )
    )


def message_view(app):
    """Return empty message view."""
    div = urwid.Filler(urwid.Divider("─"))

    command_input = QuickSwitcherWrapper(app, CommandInput())
    orig = command_input.original_widget

    def _wipe_input():
        orig.set_edit_text("")

    urwid.connect_signal(orig, "command", async_(app, app.on_command))

    urwid.connect_signal(orig, "exit", _wipe_input)

    return urwid.Pile(
        [
            ("pack", urwid.Text("#name | topic")),
            (1, div),
            urwid.ListBox(urwid.SimpleListWalker([])),
            (1, div),
            ("pack", command_input),
        ]
    )


def member_list():
    """Return empty member list"""
    return urwid.ListBox(
        urwid.SimpleFocusListWalker(
            [
                urwid.Text("member 1"),
                urwid.Text("member 2"),
                urwid.Text("member 3"),
                urwid.Text("member 4"),
                urwid.Text("member 5"),
            ]
        )
    )


def main_widget(app):
    return urwid.Columns(
        [
            # ('weight', 0.10, urwid.LineBox(guild_list(), title='guilds')),
            # ('weight', 0.35, urwid.LineBox(channel_list(), title='channels')),
            ("weight", 1, message_view(app)),
            ("weight", 0.3, urwid.LineBox(member_list(), tline="", bline="", rline="")),
        ]
    )
