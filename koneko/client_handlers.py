import urwid
import discord

from .tui.main_tree import main_widget


async def on_message(app, message):
    """Handler for every incoming message."""

    # first, push it into cache
    app.cache.push_message(message)

    # if message.channel.id == app.focus.channel_id, then we
    # must push the message to the list maintaining the message view
    if message.channel.id != app.focus.channel_id:
        return

    # get the list out of main_tree
    msg_listbox = app.msg_list_widget
    contents = msg_listbox.body.contents
    contents.append(urwid.Text(f"<{message.author.name}> {message.content}"))
    msg_listbox.set_focus(len(contents) - 1)


async def on_ready(app):
    """Call once the client becomes ready."""
    # TODO: on_ready is NOT a stable event. we need some flags
    #  stored in the app about if we were loading before, maybe some
    #  kind of... state?

    app.cancel_task("loading")
    app.loop_man.urwid_loop_final.widget = main_widget(app)


def _wrap(app, coro_func):
    """Wrap a given event handler with the app instance as an argument."""

    async def _inner(*args, **kwargs):
        return await coro_func(app, *args, **kwargs)

    return _inner


def register_discord_handlers(app):
    """Register discord event handlers."""
    # this is ugly, but works
    app.client.on_message = _wrap(app, on_message)
    app.client.on_ready = _wrap(app, on_ready)
