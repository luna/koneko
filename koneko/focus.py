import logging

from discord import TextChannel

from .utils import try_next

log = logging.getLogger(__name__)


class Focus:
    """Holds which channels the client is currently focusing on."""

    # TODO: change the widget contents upon set_* calls
    def __init__(self, app):
        self.app = app
        self.guild_id = None
        self.channel_id = None

    def _update_widgets(self, channel=None):
        """Updates, in order:
         - The channel Text widget
         - The message view ListBox (TODO)
         - The member list ListBox (TODO)
        """
        log.info("updating widgets")
        channel = channel or self.app.client.get_channel(self.channel_id)

        topic = f" | {channel.topic}" if channel.topic else " (no topic)"

        self.app.channel_status_widget.set_text(f"#{channel.name}{topic}")

    def set_guild_focus(self, guild_id: int):
        """Set the guild the client is currently focusing on.

        Automatically sets the channel focus to the first channel
        you can send messages on.
        """
        guild = self.app.client.get_guild(guild_id)

        if not guild:
            log.info("guild not found")
            return

        # TODO: a function to update the widgets once update

        self.guild_id = guild.id

        if not guild.channels:
            log.info("no channels")
            return

        # we must at least set a channel to focus on once we switch our focus
        # to a given guild.

        # the last channel id is used in client_data to store the last channel
        # the client has been on inside a guild, we load it back and restore
        # that focus when switching to a guild.
        channel_id = self.app.client_data.get_channel(guild_id)
        chan = self.app.client.get_channel(channel_id)

        # if the given last channel is not found, we fallback to either:
        #  - the first channel we can send messages to
        #  - the first channel in the guild
        # if both fail, fuck you

        if chan is None:
            chan = try_next(
                chan
                for chan in guild.text_channels
                if chan.permissions_for(guild.me).read_messages
            )

        # this is faster than doing guild.text_channels[0] and
        # blends in nicely with the previous use of try_next()
        if chan is None:
            chan = try_next(iter(guild.text_channels))

        # if channel is *still* not found...
        # TODO: just default to a fake channel, somehow, by drawing
        # a "NO CHANNEL" text, or something...
        if chan is None:
            # fuck you
            log.info("fuck you")
            return

        self._set_channel(chan)

    def _set_channel(self, chan: TextChannel):
        self.channel_id = chan.id
        self.app.client_data.set_channel(chan.guild.id, chan.id)
        self._update_widgets(channel=chan)

    def set_channel_focus(self, channel_id: int):
        """Set the channel the client is currently focusing on."""
        chan = self.app.client.get_channel(channel_id)

        if not chan:
            return

        if not isinstance(chan, TextChannel):
            return

        self._set_channel(chan)
