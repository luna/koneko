import urwid


class CommandInput(urwid.Edit):
    """Wrapper around urwid.Edit to have a 'command' signal."""

    def keypress(self, size, key: str):
        """Handle a keypress."""
        if key == "esc":
            urwid.emit_signal(self, "exit")
            return

        if key == "enter":
            # emit signal
            cur_text = self.edit_text
            urwid.emit_signal(self, "command", cur_text)
            self.set_edit_text("")
            return

        super().keypress(size, key)


urwid.register_signal(CommandInput, ["command", "exit", "change"])
