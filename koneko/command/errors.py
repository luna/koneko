class CommandError(Exception):
    """Any command error happens here."""

    pass


class CommandParseError(CommandError):
    """Parsing error class to reroute shlex errors to"""

    pass


class CommandNotFound(CommandParseError):
    """Command not found error."""

    pass
