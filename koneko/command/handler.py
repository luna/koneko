import logging
from shlex import split

from .errors import CommandNotFound

log = logging.getLogger(__name__)


class CommandHandler:
    """Command handler class."""

    def __init__(self, app):
        self.app = app

    async def call(self, command: str):
        """Parses the given command string and calls the
        proper handler for it."""
        tokens = split(command)
        command_name = tokens[0]

        # this isn't the best of ideas. this is just a test
        # a better option would be a map for proper O(1) lookup
        # instead of relying on python's attr fetch to be O(1).

        # plus, keeping it as a map gives dynamicability without
        # relying on setattr(), which might be good for future
        # plugings (maybe) trying to hook up their own commands

        # TODO: fix this
        try:
            handler = getattr(self, f"on_{command_name}")
        except:
            raise CommandNotFound()

        try:
            log.info("running command %r args %r", tokens[0], tokens[1:])
            await handler(*tokens[1:])
        except:
            log.exception("error running command %r", tokens)

    async def on_test(self):
        """Handler for /test commands."""
        log.info("test")

    async def on_qw(self):
        """Load the quickswitcher. This is mostly a test function.
        A better one would use C-k."""
        self.app.cmd_input_widget.open_pop_up()
