import asyncio
import urwid


class LoopManager:
    """Holds the asyncio and urwid loop instances."""

    def __init__(self):
        self.loop = asyncio.get_event_loop()

        # create the AsyncioEventLoop wrapper around the existing
        # asyncio loop
        self.urwid_evl = urwid.AsyncioEventLoop(loop=self.loop)

        # create main urwid_loop instance
        self._urwid_loop = None

    def urwid_loop(self, widget, palette):
        """Instantiate the urwid loop if required."""
        if not self._urwid_loop:
            self._urwid_loop = urwid.MainLoop(
                widget, palette, event_loop=self.urwid_evl, pop_ups=True
            )

        return self._urwid_loop

    @property
    def urwid_loop_final(self):
        """Raw access to the stored urwid loop."""
        return self._urwid_loop
