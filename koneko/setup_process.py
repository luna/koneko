import logging

from .config import REQUIRED_KEYS

log = logging.getLogger(__name__)

PROMPTS = {
    "discord_base_url": (
        "Please insert your Discord API base url (Litecord supported).\n"
        "(https://discordapp.com/api/v7 is Discord's base url)\n"
        "\t(keep in mind the v6 http api can work, but it is unsure"
        "to which level, as discord can change how v7 "
        "works at any moment)"
    ),
    "token": ("Insert your authentication token for the given Discord base url."),
}


def _ask_config(cfg, key: str):
    """Ask for a given config key's value."""
    prompt = PROMPTS[key]
    print(prompt)
    value = input("> ")
    cfg.set(key, value)
    cfg.save()


def cfg_setup(app):
    """Loads a simple loop asking questions to fill the configuration.

    Returns when the config is initialized.
    """
    cfg = app.config

    if cfg.initialized:
        log.info("config is initialized")
        return

    print("Your configuration file is missing required fields.")

    for key in REQUIRED_KEYS:
        if cfg.exists(key):
            continue

        _ask_config(cfg, key)

    log.info("going to the next")
    cfg_setup(app)
