import asyncio
import logging
from pathlib import Path

import urwid

from .application import Koneko
from .tui.loading import loading_widget
from .client_handlers import register_discord_handlers
from .utils import async_
from .setup_process import cfg_setup

log = logging.getLogger(__name__)


def _main_widget(app):
    # NOTE: this is a test thing only, unused for now, and exists only
    # for some degree of reference
    palette = [
        ("I say", "default,bold", "default", "bold"),
    ]
    ask = urwid.Edit(("I say", "send shit:\n"))
    reply = urwid.Text("")

    send_button = urwid.Button("send")
    exit_button = urwid.Button("exit")
    div = urwid.Divider()

    pile = urwid.Pile([ask, div, reply, div, send_button, div, exit_button])
    top = urwid.Filler(pile, valign="top")

    def on_exit_clicked(button):
        raise urwid.ExitMainLoop()

    async def on_send_button(button):
        chan = app.client.get_channel(550152697249140738)
        msg = await chan.send(ask.edit_text)
        reply.set_text(f"sent as message id {msg.id}")

    urwid.connect_signal(send_button, "click", async_(app, on_send_button))
    urwid.connect_signal(exit_button, "click", on_exit_clicked)

    return top, palette


def _setup_logging():
    # TODO: maybe /var/log/koneko or something?
    logfile = Path("koneko.log")

    file_handler = logging.FileHandler(logfile)
    file_handler.setLevel(logging.INFO)

    # setup a nicer formatter
    file_handler.setFormatter(
        logging.Formatter("[%(levelname)s] [%(name)s] %(message)s")
    )

    # catch root logger and add the handler + setting levels to info
    # so they correctly pipe out to the logfile
    root = logging.getLogger()
    root.addHandler(file_handler)
    root.setLevel(logging.INFO)


def run():
    _setup_logging()
    app = Koneko()

    # check if the config is loaded, and if not, ask user questions about
    # the config until it is.
    cfg_setup(app)

    register_discord_handlers(app)

    # by default, load the loading widget
    widget, task = loading_widget(app)

    log.info("starting!")

    try:
        # TODO: palette loading, separate from widgets
        app.start(widget, [], background_tasks=[("loading", task())])
    except KeyboardInterrupt:
        pass
    except:
        log.exception("error while running app")
    finally:
        log.info("shutting down logging..")
        logging.shutdown()
