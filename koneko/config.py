import logging
from configparser import ConfigParser
from pathlib import Path

log = logging.getLogger(__name__)

DEFAULT = """[koneko]
# the base url for discord, https://discordapp.com/api/v7, etc.
discord_base_url=

# your discord token
token=
"""

REQUIRED_KEYS = ["discord_base_url", "token"]


class Config:
    """Configuration loading"""

    def __init__(self):
        self.cfg_path = Path.home() / ".config" / "koneko.ini"

        if not self.cfg_path.exists():
            log.info("creating config")
            self.cfg_path.write_text(DEFAULT)

        self.cfg_parser = ConfigParser()
        self.cfg_parser.read(str(self.cfg_path))
        self.cfg = self.cfg_parser["koneko"]

    def get(self, key, default=None):
        """Get a key from the config."""
        return self.cfg.get(key, default)

    def __getitem__(self, key):
        return self.get(key)

    def set(self, key, value):
        """set a key on the config"""
        self.cfg_parser.set("koneko", key, value)

    def exists(self, key) -> bool:
        return bool(self.cfg[key])

    @property
    def initialized(self):
        """Return if the config is properly initialized."""
        return all(self.exists(key) for key in REQUIRED_KEYS)

    def save(self):
        """save the current configuration"""
        # TODO: make this atomic by renaming a temporary file to the
        # current path
        with self.cfg_path.open("w") as fhandle:
            self.cfg_parser.write(fhandle)
            log.info("saved config")
