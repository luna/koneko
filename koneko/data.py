import sqlite3
import logging

from pathlib import Path
from typing import Optional

log = logging.getLogger(__name__)


class ClientData:
    """Client specific data."""

    def __init__(self, app):
        self.app = app

        home = Path.home()

        # TODO: check for XDG_DATA_HOME
        path = home / ".local" / "share" / "koneko"
        path.mkdir(parents=True, exist_ok=True)

        self.conn = sqlite3.connect(path / "client_data.db")

        self._init_tables()
        self.conn.commit()

    def _init_tables(self):
        self.conn.executescript(
            """
        CREATE TABLE IF NOT EXISTS last_channel (
            guild_id bigint PRIMARY KEY,
            channel_id bigint
        );
        """
        )

        log.info("db init success")

    def get_channel(self, guild_id: int) -> Optional[int]:
        """Get a last channel ID for the given guild."""
        cur = self.conn.cursor()
        cur.execute("select channel_id from last_channel where guild_id=?", (guild_id,))
        row = cur.fetchone()

        if row is None:
            return None

        return row[0]

    def set_channel(self, guild_id: int, channel_id: int):
        """Set the last viewed channel id for a given guild."""
        self.conn.execute(
            """
        insert into
            last_channel(guild_id, channel_id)
        values
            (?, ?)
        on conflict (guild_id) do update set
            channel_id=?
            where guild_id=?
        """,
            (guild_id, channel_id, channel_id, guild_id),
        )

        self.conn.commit()
