koneko
======

an open source discord tui client, written in python3 + urwid

## install

 - need python 3.7+

```
git clone https://gitlab.com/luna/koneko.git
cd koneko
pipenv install
```

## run

```
pipenv run python koneko.py
```

 - configuration file is on `~/.config/koneko.ini`
 - database files on `~/.local/share/koneko`
 - logs on `./koneko.log`
